Installation
==============================

Require the bundle in your composer.json file:

```json
{
    "require": {
        "litalex/request-bundle": "~2.0",
        "litalex/send-request-bundle": "~1.0"
    }
}
```    

Register the bundle:

``` php
// app/AppKernel.php
public function registerBundles()
    {
        $bundles = [
            ...
            new Lvlp\NlpBundle\LvlpNlpBundle(),
            new Litalex\SendRequestBundle\LitalexSendRequestBundle()
            ...
        ];
    }
```

Usage
------

``` php
$client = new Client();
$morphologyService = $this->get('lvlp_nlp.api.morphology');
$sendRequestService = $this->get('wiseweb.send_request_service');

/**
 * Get secret_token and set to headers.
 */
$basicAuth = $morphologyService->getBasicAuthRequest();
$authTokenResponse = $sendRequestService->sendRequest($basicAuth->getServerRequest(), $client);
$token = $basicAuth->getToken($authTokenResponse);

/**
 * Get ServerRequestInterface, send it with Client and convert ResponseInterface to ResponseDto if need.
 */
$nlpMessage = $morphologyService->getUnify(["слова", "параметры"], $token);
$response = $sendRequestService->sendRequest($nlpMessage->getServerRequest(), $client);
$responseDto = $nlpMessage->convertResponse($response);
```
