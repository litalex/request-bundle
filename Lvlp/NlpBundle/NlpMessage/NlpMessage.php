<?php

namespace Lvlp\NlpBundle\NlpMessage;

use JMS\Serializer\Serializer;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class for Nlp message.
 */
class NlpMessage implements NlpMessageInterface
{
    /**
     * @var ServerRequestInterface
     */
    private $request;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @param ServerRequestInterface $request
     * @param Serializer             $serializer
     */
    public function __construct(ServerRequestInterface $request, Serializer $serializer = null)
    {
        $this->request = $request;
        $this->serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public function getServerRequest() : ServerRequestInterface
    {
        return $this->request;
    }

    /**
     * {@inheritdoc}
     */
    public function convertResponse(ResponseInterface $response) : NlpResponseDto
    {
        return $this->serializer->deserialize(
            (string) $response->getBody(),
            NlpResponseDto::class,
            'json'
        );
    }
}
