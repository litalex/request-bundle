<?php

namespace Lvlp\NlpBundle\NlpMessage;

use JMS\Serializer\Annotation as Serializer;

/**
 * NLP API response model class.
 *
 * @Serializer\ExclusionPolicy("all")
 */
class NlpResponseDto
{
    /**
     * @var string
     */
    const SESSION_KEY = 'lvlp_nlp.secret_token';

    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Expose
     */
    private $status;

    /**
     * @var array
     *
     * @Serializer\Type("array")
     * @Serializer\Expose
     */
    private $result;

    /**
     * @var array
     *
     * @Serializer\Type("array")
     * @Serializer\Expose
     */
    private $keywordsCount;

    /**
     * @var string
     *
     * @Serializer\Type("string")
     * @Serializer\Expose
     */
    private $secretToken;

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param string $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return array
     */
    public function getKeywordsCount()
    {
        return $this->keywordsCount;
    }

    /**
     * @param array $keywordsCount
     */
    public function setKeywordsCount($keywordsCount = [])
    {
        $this->keywordsCount = $keywordsCount;
    }

    /**
     * @return string
     */
    public function getSecretToken() : string
    {
        return $this->secretToken;
    }

    /**
     * @param string $secretToken
     */
    public function setSecretToken(string $secretToken)
    {
        $this->secretToken = $secretToken;
    }
}
