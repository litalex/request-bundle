<?php

namespace Lvlp\NlpBundle\NlpMessage;

use Lvlp\NlpBundle\NlpInterface;
use Lvlp\NlpBundle\Service\Interfaces\ResponseConverterInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Interface for NlpMessage.
 */
interface NlpMessageInterface extends NlpInterface, ResponseConverterInterface
{
    /**
     * Return ServerRequest.
     *
     * @return ServerRequestInterface
     */
    public function getServerRequest() : ServerRequestInterface;
}
