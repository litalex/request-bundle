<?php

namespace Lvlp\NlpBundle\Service;

use GuzzleHttp\Psr7\ServerRequest;
use JMS\Serializer\Serializer;
use Lvlp\NlpBundle\Component\BaseUrlTrait;
use Lvlp\NlpBundle\Component\BasicAuthRequestTrait;
use Lvlp\NlpBundle\NlpMessage\NlpMessage;
use Lvlp\NlpBundle\NlpMessage\NlpMessageInterface;
use Lvlp\NlpBundle\Service\Interfaces\KeywordInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Keyword service for NLP API.
 */
class Keyword implements KeywordInterface
{
    use BasicAuthRequestTrait;
    use BaseUrlTrait;

    /**
     * Filter types.
     */
    const FILTER_EXACT = 'exact';
    const FILTER_DILUTION = 'with_dilution';
    const FILTER_DILUTION_LENGTH = 'dilution_length';
    const FILTER_MORPHOLOGY = 'with_morphology';
    const FILTER_REARRANGEMENT = 'with_rearrangement';
    const FILTER_STOP_SETS = 'stop_sets';
    const FILTER_MERGE_HYPHEN = 'merge_hyphen';
    const FILTER_INCLUDE_STOPS = 'include_stops';

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    const BASE_PATH = '/api/v0.2/';

    /**
     * @param Serializer         $serializer
     * @param string             $login
     * @param string             $password
     * @param string             $host
     * @param string             $port
     */
    public function __construct(
        Serializer $serializer,
        string $login,
        string $password,
        string $host,
        string $port = null
    )
    {
        $this->serializer = $serializer;
        $this->login = $login;
        $this->password = $password;
        $this->host = $host;
        $this->port = $port;
    }

    /**
     * {@inheritdoc}
     */
    public function getKeywordsOccurrences(
        string $text,
        array $keywords,
        array $filters = [],
        $withPosition = false,
        string $token
    ) : NlpMessageInterface
    {
        return new NlpMessage(
            (new ServerRequest(
                'POST',
                $this->getBaseUrl() . 'keywords',
                ['Authorization' => $token]
            ))->withParsedBody($this->getKeywordsOccurrencesData($text, $keywords, $filters, $withPosition)),
            $this->serializer
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFilteredText(string $text, array $keywords, array $filters = [], string $token)
    : NlpMessageInterface
    {
        return new NlpMessage(
            (new ServerRequest(
                'POST',
                $this->getBaseUrl() . 'keywords_filter',
                ['Authorization' => $token]
            ))->withParsedBody($this->getKeywordsFilterData($text, $keywords, $filters)),
            $this->serializer
        );
    }

    /**
     * Get occurrences keywords data for get Request.
     *
     * @param string $text
     * @param array  $keywords
     * @param array  $filters
     * @param bool   $withPosition
     *
     * @return string
     */
    public function getKeywordsOccurrencesData($text, array $keywords, array $filters = [], $withPosition = false)
    {
        $filters = $this->getFilters($filters);
        $data = [
            'text' => $text,
            'keywords' => $keywords,
            Keyword::FILTER_EXACT => $filters[Keyword::FILTER_EXACT],
            Keyword::FILTER_DILUTION => $filters[Keyword::FILTER_DILUTION],
            Keyword::FILTER_MORPHOLOGY => $filters[Keyword::FILTER_MORPHOLOGY],
            Keyword::FILTER_REARRANGEMENT => $filters[Keyword::FILTER_REARRANGEMENT],
            Keyword::FILTER_MERGE_HYPHEN => $filters[Keyword::FILTER_MERGE_HYPHEN],
            Keyword::FILTER_INCLUDE_STOPS => $filters[Keyword::FILTER_INCLUDE_STOPS],
            'with_position' => $withPosition,
        ];
        if ($filters[Keyword::FILTER_DILUTION_LENGTH]) {
            $data[Keyword::FILTER_DILUTION_LENGTH] = $filters[Keyword::FILTER_DILUTION_LENGTH];
        }
        if (!empty($filters[Keyword::FILTER_STOP_SETS])) {
            $data[Keyword::FILTER_STOP_SETS] = $filters[Keyword::FILTER_STOP_SETS];
        }

        return $data;
    }

    /**
     * Returns text filtered by keywords due to settings.
     *
     * @param string $text
     * @param array  $keywords
     * @param array  $filters
     *
     * @return array
     */
    private function getKeywordsFilterData(string $text, array $keywords, array $filters = [])
    {
        $filters = $this->getFilters($filters);
        $data = [
            'text' => $text,
            'keywords' => $keywords,
            Keyword::FILTER_EXACT => $filters[Keyword::FILTER_EXACT],
            Keyword::FILTER_DILUTION => $filters[Keyword::FILTER_DILUTION],
            Keyword::FILTER_MORPHOLOGY => $filters[Keyword::FILTER_MORPHOLOGY],
            Keyword::FILTER_REARRANGEMENT => $filters[Keyword::FILTER_REARRANGEMENT],
        ];
        if ($filters[Keyword::FILTER_DILUTION_LENGTH]) {
            $data[Keyword::FILTER_DILUTION_LENGTH] = $filters[Keyword::FILTER_DILUTION_LENGTH];
        }
        if (!empty($filters[Keyword::FILTER_STOP_SETS])) {
            $data[Keyword::FILTER_STOP_SETS] = $filters[Keyword::FILTER_STOP_SETS];
        }

        return $data;
    }

    /**
     * Generates filters for request.
     *
     * @param array $filters
     *
     * @return array
     */
    private function getFilters(array $filters)
    {
        $defaultFilters = [
            self::FILTER_EXACT => true,
            self::FILTER_DILUTION => false,
            self::FILTER_DILUTION_LENGTH => 0,
            self::FILTER_MORPHOLOGY => false,
            self::FILTER_REARRANGEMENT => false,
            self::FILTER_STOP_SETS => [],
            self::FILTER_MERGE_HYPHEN => false,
            self::FILTER_INCLUDE_STOPS => false,
        ];

        return array_merge($defaultFilters, $filters);
    }
}
