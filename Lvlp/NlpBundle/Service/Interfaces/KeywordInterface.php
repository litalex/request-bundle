<?php

namespace Lvlp\NlpBundle\Service\Interfaces;

/**
 * Interface for Keyword service.
 */
interface KeywordInterface extends KeywordsOccurrencesInterface, KeywordFilteredInterface
{
}
