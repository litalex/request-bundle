<?php

namespace Lvlp\NlpBundle\Service\Interfaces;

use Lvlp\NlpBundle\NlpInterface;
use Lvlp\NlpBundle\NlpMessage\NlpMessageInterface;
use Psr\Http\Message\RequestInterface;

/**
 * Interface for keyword filtered.
 */
interface KeywordFilteredInterface extends NlpInterface
{
    /**
     * Returns Request for text filtered by keywords due to settings.
     *
     * @param string $text
     * @param string $token
     * @param array  $keywords
     * @param array  $filters
     *
     * @return NlpMessageInterface
     */
    public function getFilteredText(string $text, string $token, array $keywords, array $filters = [])
    : NlpMessageInterface;
}
