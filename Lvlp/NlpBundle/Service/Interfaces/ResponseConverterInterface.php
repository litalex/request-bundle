<?php

namespace Lvlp\NlpBundle\Service\Interfaces;

use Lvlp\NlpBundle\NlpMessage\NlpResponseDto;
use Psr\Http\Message\ResponseInterface;

/**
 * Interface for convert ResponseInterface to NlpMessage.
 */
interface ResponseConverterInterface
{
    /**
     * Convert ResponseInterface to NlpMessage.
     *
     * @param ResponseInterface $response
     *
     * @return NlpResponseDto
     */
    public function convertResponse(ResponseInterface $response) : NlpResponseDto;
}
