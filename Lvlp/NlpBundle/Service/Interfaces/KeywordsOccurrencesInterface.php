<?php

namespace Lvlp\NlpBundle\Service\Interfaces;

use Lvlp\NlpBundle\NlpInterface;
use Lvlp\NlpBundle\NlpMessage\NlpMessageInterface;
use Psr\Http\Message\RequestInterface;

/**
 * Interface for keyword occurrences.
 */
interface KeywordsOccurrencesInterface extends NlpInterface
{
    /**
     * Returns Request for get occurrences keywords due to settings.
     *
     * @param string $text
     * @param array  $keywords
     * @param string $token
     * @param array  $filters
     * @param bool   $withPosition
     *
     * @return NlpMessageInterface
     */
    public function getKeywordsOccurrences(
        string $text,
        array $keywords,
        string $token,
        array $filters = [],
        $withPosition = false
    ) : NlpMessageInterface;
}
