<?php

namespace Lvlp\NlpBundle\Service\Interfaces;

use Lvlp\NlpBundle\NlpInterface;
use Lvlp\NlpBundle\NlpMessage\NlpMessageInterface;

/**
 * Interface for get decline phrase.
 */
interface DeclinePhraseInterface extends NlpInterface
{
    /**
     * Returns Request for get declined phrase by given parameters.
     *
     * @param string $phrase
     * @param string $gender
     * @param string $number
     * @param string $case
     * @param string $token
     *
     * @return NlpMessageInterface
     */
    public function declinePhrase(string $phrase, string $gender, string $number, string $case, string $token)
    : NlpMessageInterface;
}
