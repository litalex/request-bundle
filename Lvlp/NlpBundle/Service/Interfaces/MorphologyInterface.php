<?php

namespace Lvlp\NlpBundle\Service\Interfaces;

/**
 * Interface for Morphology service.
 */
interface MorphologyInterface extends UnifyInterface, DeclinePhraseInterface
{
}
