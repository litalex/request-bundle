<?php

namespace Lvlp\NlpBundle\Service\Interfaces;

use Lvlp\NlpBundle\NlpInterface;
use Lvlp\NlpBundle\NlpMessage\NlpMessageInterface;

/**
 * Interface for get unify forms for words.
 */
interface UnifyInterface extends NlpInterface
{
    /**
     * Returns Request for get normal form for each word in input array.
     *
     * @param array  $words
     * @param string $token
     * @param bool   $root
     * @param bool   $fixNumber
     *
     * @return NlpMessageInterface
     */
    public function getUnify(array $words, string $token, bool $root = false, bool $fixNumber = false)
    : NlpMessageInterface;
}
