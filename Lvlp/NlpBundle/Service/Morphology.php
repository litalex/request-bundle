<?php

namespace Lvlp\NlpBundle\Service;

use GuzzleHttp\Psr7\ServerRequest;
use JMS\Serializer\Serializer;
use Lvlp\NlpBundle\Component\BaseUrlTrait;
use Lvlp\NlpBundle\Component\BasicAuthRequestTrait;
use Lvlp\NlpBundle\NlpMessage\NlpMessage;
use Lvlp\NlpBundle\NlpMessage\NlpMessageInterface;
use Lvlp\NlpBundle\Service\Interfaces\MorphologyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class Morphology for work with word forms.
 */
class Morphology implements MorphologyInterface
{
    use BasicAuthRequestTrait;
    use BaseUrlTrait;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    const BASE_PATH = '/api/v0.2/';

    /**
     * @param Serializer         $serializer
     * @param string             $login
     * @param string             $password
     * @param string             $host
     * @param string             $port
     */
    public function __construct(
        Serializer $serializer,
        string $login,
        string $password,
        string $host,
        string $port = null
    )
    {
        $this->serializer = $serializer;
        $this->login = $login;
        $this->password = $password;
        $this->host = $host;
        $this->port = $port;
    }

    /**
     * {@inheritdoc}
     */
    public function getUnify(array $words, string $token, bool $root = false, bool $fixNumber = false) : NlpMessageInterface
    {
        return new NlpMessage(
            (new ServerRequest(
                'POST',
                $this->getBaseUrl() . 'morphling/unify',
                ['Authorization' => $token]
            ))->withParsedBody([
                'text' => $words,
                'root' => $root,
                'fix_number' => $fixNumber,
            ]),
            $this->serializer
        );
    }

    /**
     * {@inheritdoc}
     */
    public function declinePhrase(string $phrase, string $gender, string $number, string $case, string $token)
    : NlpMessageInterface
    {
        return new NlpMessage(
            (new ServerRequest(
                'POST',
                $this->getBaseUrl() . 'morphling/decline_subphrases',
                ['Authorization' => $token]
            ))->withParsedBody($this->getDeclinePhraseData($phrase, $gender, $number, $case)),
            $this->serializer
        );
    }

    /**
     * Get post data for get declined phrase Request.
     *
     * @param string $phrase
     * @param string $gender
     * @param string $number
     * @param string $case
     *
     * @return string
     */
    private function getDeclinePhraseData($phrase, $gender, $number, $case)
    {
        $gramFormArray = [];
        if ($gender) {
            $gramFormArray[] = $gender;
        }
        if ($number) {
            $gramFormArray[] = $number;
        }
        if ($case) {
            $gramFormArray[] = $case;
        }

        return [
            'text' => $phrase,
            'grammemes' => $gramFormArray,
        ];
    }
}
