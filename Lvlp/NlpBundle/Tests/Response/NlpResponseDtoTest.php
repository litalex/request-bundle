<?php

namespace Lvlp\NlpBundle\Tests\Response;

use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use JMS\Serializer\Serializer;
use Lvlp\NlpBundle\NlpMessage\NlpMessage;
use Lvlp\NlpBundle\NlpMessage\NlpResponseDto;
use Lvlp\NlpBundle\Service\Keyword;
use Lvlp\Symfony2TestCase\ValueObjectTesterTrait;

/**
 * NLP API simple response model test class.
 *
 * @see \PHPUnit_Framework_TestCase
 */
class NlpResponseDtoTest extends \PHPUnit_Framework_TestCase
{
    use ValueObjectTesterTrait;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * Basic setup.
     */
    public function setUp()
    {
        $this->serializer = $this->getMockBuilder('JMS\Serializer\Serializer')
            ->disableOriginalConstructor()->getMock();
    }

    /**
     * @return void
     */
    public function testGettersSetters()
    {
        $this->gettersSettersTester(new NlpResponseDto(), [
            'status',
            'result',
            'keywordsCount',
        ]);
    }

    /**
     * Test convertResponse method.
     */
    public function testConvertResponse()
    {
        $status = '200';
        $jsonResult = '{"status":"200","result":"fd26a7f6-e83c-4db7-8eff-ae80b8d89511","keywordsCount":["88","888"]}';

        $returnedResponse = new NlpResponseDto();
        $returnedResponse->setStatus($status);
        $returnedResponse->setResult('fd26a7f6-e83c-4db7-8eff-ae80b8d89511');
        $returnedResponse->setKeywordsCount([88, 888]);

        $this->serializer->expects($this->once())
            ->method('deserialize')
            ->with($this->equalTo($jsonResult), $this->equalTo(NlpResponseDto::class), $this->equalTo('json'))
            ->will($this->returnValue($returnedResponse));

        $guzzleResponse = new Response($status, [], $jsonResult);
        $nlpMessage = new NlpMessage(
            new ServerRequest(
                'POST',
                'uri'
            ),
            $this->serializer
        );

        $nlpResponse = ($nlpMessage)
            ->convertResponse($guzzleResponse);

        $this->assertInstanceOf(NlpResponseDto::class, $nlpResponse);
        $this->assertEquals($returnedResponse, $nlpResponse);
        $this->assertEquals($returnedResponse->getStatus(), $nlpResponse->getStatus());
        $this->assertEquals($returnedResponse->getResult(), $nlpResponse->getResult());
    }
}
