<?php

namespace Lvlp\NlpBundle\Tests\Service;

use JMS\Serializer\Serializer;
use Lvlp\NlpBundle\NlpMessage\NlpMessageInterface;
use Lvlp\NlpBundle\Service\Interfaces\MorphologyInterface;
use Lvlp\NlpBundle\Service\Morphology;

/**
 * Class for test MorphologyService.
 */
class MorphologyTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var MorphologyInterface
     */
    private $service;

    /**
     * Basic setup.
     */
    public function setUp()
    {
        $this->serializer = $this->getMockBuilder('JMS\Serializer\Serializer')
            ->disableOriginalConstructor()->getMock();

        $this->service = $this->createService();
    }

    /**
     * Get unify method test.
     */
    public function testGetUnify()
    {
        $words = [uniqid(), uniqid()];
        $token = uniqid();

        $request = $this->service->getUnify($words, $token);
        $resultBodyArray = $request->getServerRequest()->getParsedBody();

        $this->assertInstanceOf(NlpMessageInterface::class, $request);
        $this->assertEquals($words, $resultBodyArray['text']);
    }

    /**
     * Get unify method test.
     */
    public function testDeclinePhrase()
    {
        $string = uniqid();
        $token = uniqid();

        $request = $this->service->declinePhrase($string, $string, $string, $string, $token);
        $resultBodyArray = $request->getServerRequest()->getParsedBody();

        $this->assertInstanceOf(NlpMessageInterface::class, $request);
        $this->assertEquals($string, $resultBodyArray['text']);
    }

    /**
     * Create service.
     *
     * @return Morphology
     */
    protected function createService()
    {
        return new Morphology(
            $this->serializer,
            uniqid(),
            uniqid(),
            uniqid(),
            uniqid());
    }
}
