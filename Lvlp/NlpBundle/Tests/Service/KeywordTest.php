<?php

namespace Lvlp\NlpBundle\Tests\Service;

use JMS\Serializer\Serializer;
use Lvlp\NlpBundle\NlpMessage\NlpMessageInterface;
use Lvlp\NlpBundle\Service\Interfaces\KeywordInterface;
use Lvlp\NlpBundle\Service\Keyword;

/**
 * Class for tests KeywordService.
 */
class KeywordTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var KeywordInterface
     */
    private $service;

    /**
     * Basic setup.
     */
    public function setUp()
    {
        $this->serializer = $this->getMockBuilder('JMS\Serializer\Serializer')
            ->disableOriginalConstructor()->getMock();

        $this->service = $this->createService();
    }

    /**
     * Get filtered text method test.
     */
    public function testGetFilteredText()
    {
        $text = uniqid();
        $keywords = [uniqid(), uniqid()];
        $token = uniqid();

        $request = $this->service->getFilteredText($text, $keywords, $token);
        $resultBodyArray = $request->getServerRequest()->getParsedBody();

        $this->assertInstanceOf(NlpMessageInterface::class, $request);
        $this->assertEquals($text, $resultBodyArray['text']);
        $this->assertEquals($keywords, $resultBodyArray['keywords']);
    }

    /**
     * Get keywords occurrences method test.
     */
    public function testGetKeywordsOccurrences()
    {
        $text = uniqid();
        $keywords = [uniqid(), uniqid()];
        $token = uniqid();

        $request = $this->service->getKeywordsOccurrences($text, $keywords, $token);
        $resultBodyArray = $request->getServerRequest()->getParsedBody();

        $this->assertInstanceOf(NlpMessageInterface::class, $request);
        $this->assertEquals($text, $resultBodyArray['text']);
        $this->assertEquals($keywords, $resultBodyArray['keywords']);
    }

    /**
     * Creates service.
     *
     * @return Keyword
     */
    protected function createService()
    {
        return new Keyword(
            $this->serializer,
            uniqid(),
            uniqid(),
            uniqid(),
            uniqid()
        );
    }
}
