<?php

namespace Lvlp\NlpBundle\Component;

use JMS\Serializer\Serializer;
use Lvlp\NlpBundle\NlpMessage\NlpResponseDto;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class for Basic HTTP Auth.
 */
class BasicAuth implements BasicAuthInterface
{
    /**
     * @var ServerRequestInterface
     */
    private $request;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @param ServerRequestInterface $request
     * @param Serializer             $serializer
     */
    public function __construct(ServerRequestInterface $request, $serializer)
    {
        $this->request = $request;
        $this->serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public function getServerRequest() : ServerRequestInterface
    {
        return $this->request;
    }

    /**
     * {@inheritdoc}
     */
    public function getToken(ResponseInterface $response)
    {
        /*** @var NlpResponseDto */
        $responseDto = $this->serializer->deserialize(
            (string) $response->getBody(),
            NlpResponseDto::class,
            'json'
        );

        return $responseDto->getSecretToken();
    }
}
