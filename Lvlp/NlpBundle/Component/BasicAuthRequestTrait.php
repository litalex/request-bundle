<?php

namespace Lvlp\NlpBundle\Component;

use GuzzleHttp\Psr7\ServerRequest;

/**
 * Trait for basic auth request.
 */
trait BasicAuthRequestTrait
{
    /**
     * Returns Request of basic auth for get auth token by API.
     *
     * @return BasicAuth
     */
    public function getBasicAuthRequest() : BasicAuth
    {
        return new BasicAuth(
            (new ServerRequest(
                'POST',
                $this->getBaseUrl() . 'authenticate'
            ))
                ->withParsedBody(
                    [
                        'login' => $this->login,
                        'password' => $this->password
                    ]
                ),
            $this->serializer
        );
    }
}
