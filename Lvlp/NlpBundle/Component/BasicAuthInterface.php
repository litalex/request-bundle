<?php

namespace Lvlp\NlpBundle\Component;

use Psr\Http\Message\ResponseInterface;

/**
 * Interface for Basic Authentication.
 */
interface BasicAuthInterface extends AuthenticationInterface
{
    /**
     * Get secret_token for basic auth from Response.
     *
     * @param ResponseInterface $response
     *
     * @return string
     */
    public function getToken(ResponseInterface $response);
}
