<?php

namespace Lvlp\NlpBundle\Component;

/**
 * Class for prepare BaseUrl.
 */
trait BaseUrlTrait
{
    /**
     * Returns base url for request.
     *
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->getHost() .  self::BASE_PATH;
    }

    /**
     * Returns host for api Request.
     *
     * @return string
     */
    private function getHost()
    {
        return $this->port ? $this->host . ':' . $this->port : $this->host;
    }
}
