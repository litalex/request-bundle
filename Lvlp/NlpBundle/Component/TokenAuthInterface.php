<?php

namespace Lvlp\NlpBundle\Component;

/**
 * Interface for Token Authentication.
 */
interface TokenAuthInterface extends AuthenticationInterface
{
}
